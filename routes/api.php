<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('/user-profile', [AuthController::class, 'userProfile'])->middleware('jwt.verify');

Route::group([
    'middleware' => ['api' ,'cors'],
    'prefix' => 'auth'

], function () {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout'])->middleware('jwt.verify');
    Route::get('/user-profile', [AuthController::class, 'userProfile'])->middleware('jwt.verify');
    Route::post('/change-pass', [AuthController::class, 'changePassWord'])->middleware('jwt.verify');  
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::post('/forgot-password', [AuthController::class, 'forgotPassword']);
    Route::get('/reset-password/{token}', [AuthController::class, 'resetPassword']);
    Route::post('/reset-password', [AuthController::class, 'resetPasswordProcess']);
    
});
