<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Support\Facades\Validator;
use App\Jobs\SendForgotPasswordEmail;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    //
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('api', ['except' => ['login', 'register', 'forgotPassword', 'resetPassword', 'resetPasswordProcess']]);
    }

    public function login(Request $request)
    {
        $data = $request->all();
        $user = User::where('email', $data['email'])->orWhere('name', $data['email'])->first();

        if (!$user) {
            return response()->json(['message' => 'Invalid login credentials'], 404);
        }

        $validator = Validator::make($data, [
            'password' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $token = auth('api')->attempt(['name' => $data['email'], 'password' => $data['password']]);

        if (!$token) {
            $token = auth('api')->attempt(['email' => $data['email'], 'password' => $data['password']]);
        }

        if (!$token) {
            return response()->json(['message' => 'Invalid login credentials'], 401);
        }

        $refreshToken = $this->createRefreshToken();

        return $this->createNewToken($token, $refreshToken);
    }
    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::create(array_merge(
            $validator->validated(),
            ['password' => bcrypt($request->password)]
        ));

        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        try {
            $refreshToken = request()->refresh_token;
            $decode = JWTAuth::getJWTProvider()->decode($refreshToken);
            if ($decode['exp'] < time()) {
                return response()->json(['message' => 'Expired token'], 401);
            }
            // dd($decode['userId']);
            $user = User::find($decode['userId']);
            if (!$user) {
                return response()->json(['message' => 'Invalid refresh token'], 401);
            };

            $token = auth('api')->login($user);
            $refreshToken = $this->createRefreshToken();

            return $this->createNewToken($token, $refreshToken);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Invalid refresh token'], 401);
        }
    }

    public function userProfile()
    {
        try {
            return response()->json(auth('api')->user());
        } catch (JWTException $e) {
            return response()->json(['error' => 'Something went wrong'], 500);
        }
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function createNewToken($token, $refreshToken = null)
    {
        return response()->json([
            'access_token' => $token,
            'refresh_token' => $refreshToken,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 1,
            // 'user' => auth('api')->user()
        ]);
    }

    private function createRefreshToken()
    {
        $data = [
            'userId' => auth('api')->user()->id,
            'random' => rand() . time(),
            'exp' => time() + config('jwt.refresh_ttl'),
        ];

        $refreshToken = JWTAuth::getJWTProvider()->encode($data);
        return $refreshToken;
    }

    public function changePassWord(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'old_password' => 'required|string|min:6',
            'password' => 'required|string|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $userId = auth('api')->user()->id;

        $user = User::where('id', $userId)->first();

        if (Hash::check($data['old_password'], $user->password)) {
            // Right password
            $user->update(
                ['password' => bcrypt($data['password'])]
            );
        } else {
            return response()->json(['message' => 'Invalid old password'], 400);
        }

        return response()->json([
            'message' => 'User successfully changed password',
            'user' => $user,
        ], 200);
    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:100',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        }

        $data = [
            'email' => $user->email,
            'exp' => time() + 300,
        ];
        $encrypted = $this->encrypt($data);

        $valueEmail = [
            'email' => $user->email,
            'name' => $user->name,
            'token' => $encrypted,
        ];

        SendForgotPasswordEmail::dispatch($valueEmail);

        return response()->json([
            'message' => 'Reset password link sent to your email',
        ], 200);
    }

    public function resetPassword($token)
    {

        try {
            $user = $this->verifyToken($token);

            return response()->json([
                'message' => 'Token valid',
                'user' => $user,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Invalid token'], 401);
        }
    }

    public function resetPasswordProcess(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make($data, [
                'token' => 'required|string',
                'password' => 'required|string|confirmed|min:6',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $user = $this->verifyToken($data['token']);

            $user = User::where('email', $user['email'])->update(
                ['password' => bcrypt($data['password'])]
            );

            return response()->json([
                'message' => 'User successfully changed password',
                'user' => $user,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Invalid token'], 401);
        }
    }

    private function encrypt($data)
    {
        if ($data) {
            $encrypted = encrypt($data, true);
            return base64_encode($encrypted);
        }
    }

    private function decrypt($data)
    {
        if ($data) {
            $decrypted = decrypt(base64_decode($data), true);
            return $decrypted;
        }
    }

    private function verifyToken($token)
    {
        $data = $this->decrypt($token, env('APP_KEY'));

        if ($data['exp'] < time()) {
            return response()->json(['message' => 'Expired token'], 401);
        }

        $user = User::where('email', $data['email'])->first();
        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        }

        return $user;
    }
}
